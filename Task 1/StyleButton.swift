//
//  StyleButton.swift
//  Task 1
//
//  Created by Sergey Parfentchik on 5.02.22.
//

import UIKit

class StyleButton: UIButton {

  init(title: String) {
    super.init(frame: .zero)
    
    setTitle(title, for: .normal)
    setTitleColor(UIColor(named: "TitleButtonColor"), for: .normal)
    titleLabel?.lineBreakMode = .byWordWrapping
    
    backgroundColor = UIColor.systemBackground
    
    layer.shadowColor = UIColor.systemGray2.cgColor
    layer.shadowOpacity = 1
    layer.shadowRadius = 2
    layer.shadowOffset = CGSize.init(width: 0, height: 0)
    
    translatesAutoresizingMaskIntoConstraints = false
    
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
