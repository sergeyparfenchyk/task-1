//
//  SceneDelegate.swift
//  Task 1
//
//  Created by Sergey Parfentchik on 5.02.22.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

  var window: UIWindow?

  func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
    guard let windowScene = (scene as? UIWindowScene) else { return }
    window = UIWindow(frame: windowScene.coordinateSpace.bounds)
    settingAppearance()
    settingLanguage()
    window?.windowScene = windowScene
    window?.rootViewController = ViewController()
    window?.makeKeyAndVisible()
  }

  func sceneDidDisconnect(_ scene: UIScene) {
    
  }

  func sceneDidBecomeActive(_ scene: UIScene) {
    
  }

  func sceneWillResignActive(_ scene: UIScene) {
    
  }

  func sceneWillEnterForeground(_ scene: UIScene) {
    
  }

  func sceneDidEnterBackground(_ scene: UIScene) {
    
  }
  
  //MARK: - settingAppearance
  
  private func settingAppearance() {
    let defaults = UserDefaults.standard
    switch defaults.integer(forKey: "Appearance") {
    case 0:
      window?.overrideUserInterfaceStyle = .unspecified
    case 1:
      window?.overrideUserInterfaceStyle = .light
    case 2:
      window?.overrideUserInterfaceStyle = .dark
    default:
      break
    }
  }

  //MARK: - settingLanguage
  
  private func settingLanguage() {
    let defaults = UserDefaults.standard
    let language = defaults.integer(forKey: "Language") - 1
    if language >= 0 && language < 3 {
      switch language {
      case 0:
        Bundle.setLanguage("en")
      case 1:
        Bundle.setLanguage("be")
      case 2:
        Bundle.setLanguage("ru")
      default:
        break
      }
    }
  }
}

