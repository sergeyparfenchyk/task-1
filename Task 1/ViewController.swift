//
//  ViewController.swift
//  Task 1
//
//  Created by Sergey Parfentchik on 5.02.22.
//

import UIKit

class ViewController: UIViewController {
  
  // MARK: - Private Properties
  
  private let logoImageView = UIImageView(image: UIImage(named: "Logo"))
  
  private let greetingTextLabel = UITextView()
  
  private let lightStyleButton = StyleButton(title: NSLocalizedString("Light", comment: ""))
  private let darkStyleButton = StyleButton(title: NSLocalizedString("Dark", comment: ""))
  private let autoStyleButton = StyleButton(title: NSLocalizedString("Auto", comment: ""))
  
  private let languagePickerView = UIPickerView()
  private var languages: [String]!
  
  // MARK: - Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = UIColor.systemBackground
    
    languagePickerView.delegate = self
    languagePickerView.dataSource = self
    
    settingLogoView()
    settingGreetingText()
    settingButton()
    settingLanguagePicker()
    
    settingContraints()
  }
  
  // MARK: - Private Methods
  
  private func settingLogoView(){
    view.addSubview(logoImageView)
    logoImageView.translatesAutoresizingMaskIntoConstraints = false
  }
  
  private func settingGreetingText(){
    greetingTextLabel.font = UIFont.systemFont(ofSize: 18)
    greetingTextLabel.text = NSLocalizedString("Greeting Text", comment: "")
    greetingTextLabel.layer.cornerRadius = 10
    greetingTextLabel.isEditable = false
    greetingTextLabel.isScrollEnabled = false
    greetingTextLabel.backgroundColor = UIColor.systemGray6
    view.addSubview(greetingTextLabel)
    greetingTextLabel.translatesAutoresizingMaskIntoConstraints = false
  }
  
  private func settingButton(){
    view.addSubview(autoStyleButton)
    autoStyleButton.tag = 0
    autoStyleButton.addTarget(self, action: #selector(touchUpInsideButton), for: .touchUpInside)
    
    view.addSubview(lightStyleButton)
    lightStyleButton.tag = 1
    lightStyleButton.addTarget(self, action: #selector(touchUpInsideButton), for: .touchUpInside)
    
    view.addSubview(darkStyleButton)
    darkStyleButton.tag = 2
    darkStyleButton.addTarget(self, action: #selector(touchUpInsideButton), for: .touchUpInside)
    
    let defaults = UserDefaults.standard
    switch defaults.integer(forKey: "Appearance") {
    case 0:
      autoStyleButton.backgroundColor = UIColor.systemGray2
    case 1:
      lightStyleButton.backgroundColor = UIColor.systemGray2
    case 2:
      darkStyleButton.backgroundColor = UIColor.systemGray2
    default:
      break
    }
  }
  
  private func settingLanguagePicker(){
    let englishLanguage = NSLocalizedString("English", comment: "")
    let belarusianLanguage = NSLocalizedString("Belarusian", comment: "")
    let russianLanguage = NSLocalizedString("Russian", comment: "")
    
    languages = [englishLanguage, belarusianLanguage, russianLanguage]
    view.addSubview(languagePickerView)
    languagePickerView.translatesAutoresizingMaskIntoConstraints = false
    
    let defaults = UserDefaults.standard
    let language = defaults.integer(forKey: "Language") - 1
    if language >= 0 && language < 3 {
      languagePickerView.selectRow(language, inComponent: 0, animated: false)
    } else {
      let langStr = Locale.current.languageCode
      switch langStr {
      case "en":
        languagePickerView.selectRow(0, inComponent: 0, animated: false)
      case "be":
        languagePickerView.selectRow(1, inComponent: 0, animated: false)
      case "ru":
        languagePickerView.selectRow(2, inComponent: 0, animated: false)
      default:
        break
      }
    }
  }
  
  private func settingContraints(){
    NSLayoutConstraint.activate([
      darkStyleButton.bottomAnchor.constraint(equalTo: languagePickerView.topAnchor),
      darkStyleButton.heightAnchor.constraint(equalTo: lightStyleButton.heightAnchor),
      darkStyleButton.heightAnchor.constraint(equalToConstant: 45),
           
      lightStyleButton.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 4),
      lightStyleButton.centerYAnchor.constraint(equalTo: darkStyleButton.centerYAnchor),
      lightStyleButton.rightAnchor.constraint(equalTo: darkStyleButton.leftAnchor, constant: -4),
      lightStyleButton.widthAnchor.constraint(equalTo: darkStyleButton.widthAnchor),
      lightStyleButton.heightAnchor.constraint(equalTo: darkStyleButton.heightAnchor),
            
      autoStyleButton.centerYAnchor.constraint(equalTo: darkStyleButton.centerYAnchor),
      autoStyleButton.leftAnchor.constraint(equalTo: darkStyleButton.rightAnchor, constant: 4),
      autoStyleButton.widthAnchor.constraint(equalTo: darkStyleButton.widthAnchor),
      autoStyleButton.heightAnchor.constraint(equalTo: darkStyleButton.heightAnchor),
      autoStyleButton.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -4),
            
      languagePickerView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      languagePickerView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 45),
            
      logoImageView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 10),
      logoImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
      logoImageView.widthAnchor.constraint(equalToConstant: 48),
            
      greetingTextLabel.centerYAnchor.constraint(equalTo: logoImageView.centerYAnchor),
      greetingTextLabel.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -4),
      greetingTextLabel.leftAnchor.constraint(equalTo: logoImageView.rightAnchor, constant: 10),
    ])
  }
}

// MARK: - UIPickerViewDataSource

extension ViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return languages.count
    }
}

// MARK: - UIPickerViewDelegate

extension ViewController: UIPickerViewDelegate {
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
      return languages[row]
  }
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    let defaults = UserDefaults.standard
    defaults.set(row + 1, forKey: "Language")
    switch row {
    case 0:
      Bundle.setLanguage("en")
    case 1:
      Bundle.setLanguage("be")
    case 2:
      Bundle.setLanguage("ru")
    default:
      break
    }
    self.view.window?.rootViewController = ViewController()
  }
}

// MARK: - TouchUpInsideButton

extension ViewController {
  @objc func touchUpInsideButton(button: UIButton) {
    lightStyleButton.backgroundColor = UIColor.systemBackground
    darkStyleButton.backgroundColor = UIColor.systemBackground
    autoStyleButton.backgroundColor = UIColor.systemBackground
    button.backgroundColor = UIColor.systemGray2
    let defaults = UserDefaults.standard
    switch button.tag {
    case 0:
      defaults.set(0, forKey: "Appearance")
      self.view.window?.overrideUserInterfaceStyle = .unspecified
    case 1:
      defaults.set(1, forKey: "Appearance")
      self.view.window?.overrideUserInterfaceStyle = .light
    case 2:
      defaults.set(2, forKey: "Appearance")
      self.view.window?.overrideUserInterfaceStyle = .dark
    default:
      break
    }
  }
}



